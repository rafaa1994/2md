<?php

namespace Md\Integration\Command;

use Md\Integration\Helper\Comparer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StringComparer
 * @package Md\Integration\Command
 */
class StringComparer extends Command
{
    /**
     * {@inheritdoc}
     */
    public function __construct($name = null)
    {
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('md:comparer')
            ->setDescription('Find out whether params containing number in provided haystack are the same')
            ->addArgument('haystack', InputArgument::REQUIRED, 'String for check')
            ->addArgument('needles', InputArgument::IS_ARRAY, 'Optional parameters. Default is set "Mary" and "John"');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $haystack = $input->getArgument('haystack');
        $needles = $input->getArgument('needles');

        if (empty($needles)) {
            $needles = ['Mary', 'John'];
        }

        $equal = (int) Comparer::equalSubstringNumber($haystack, $needles);
        $output->writeln("Number of times is the same : $equal");
    }
}
