<?php

namespace Md\Integration\Command;

use Md\Integration\Helper\Sorter;
use Md\Integration\Supplier\SupplierFactoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SortingJSON
 * @package Md\Integration\Command
 */
class SortingJSON extends Command
{

    /**
     * @var $supplierFactory SupplierFactoryInterface
     */
    public $supplierFactory;

    /**
     * {@inheritdoc}
     */
    public function __construct($name = null, SupplierFactoryInterface $supplierFactory)
    {
        parent::__construct($name);
        $this->supplierFactory = $supplierFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('md:sorting')
            ->setDescription('Sorting array in JSON by price or title')
            ->addArgument('mode', InputArgument::REQUIRED, "1 --String in JSON param\t2 --Path to JSON file")
            ->addArgument('supplier', InputArgument::REQUIRED, 'Data param');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        try {
            $supplier = $this->supplierFactory->getSupplier($input->getArgument('mode'));
            $supplier->setContent($input->getArgument('supplier'));

            $table = new Table($output);
            $table->setHeaders(['title', 'price', 'inventory']);
            $table->addRows($supplier->getProducts());
            $table->render();
            $output->writeln("\nJSON format\n".json_encode($supplier->getProducts()));
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}