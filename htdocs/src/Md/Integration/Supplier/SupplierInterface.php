<?php

namespace Md\Integration\Supplier;

/**
 * Interface SupplierInterface
 * @package Md\Integration\Supplier
 */
interface SupplierInterface
{

    /**
     * Get products array
     *
     * @return array
     */
    public function getProducts();

    /**
     * Get supplier name
     *
     * @return string
     */
    public static function getMode();

    /**
     * Get response type
     *
     * @return string
     */
    public static function getResponseType();

    /**
     * Set content
     *
     * @param string $content
     * @return mixed
     */
    public function setContent($content);
}
