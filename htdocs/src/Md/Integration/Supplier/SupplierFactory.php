<?php

namespace Md\Integration\Supplier;

use Symfony\Component\Console\Exception\InvalidArgumentException;

/**
 * Class SupplierFactory
 * @package Md\Integration\Supplier
 */
class SupplierFactory implements SupplierFactoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function getSupplier($mode)
    {
        $supplier = null;
        switch ($mode) {
            case PathSupplier::getMode():
                $supplier = new PathSupplier();
                break;
            case StringSupplier::getMode():
                $supplier = new StringSupplier();
                break;
            default:
                throw new InvalidArgumentException("$mode is not supported yet!");
        }

        return $supplier;
    }
}
