<?php

namespace Md\Integration\Supplier;

/**
 * Class StringSupplier
 * @package Md\Integration\Supplier
 */
class StringSupplier extends SupplierAbstract
{

    const STRING_MODE = 1;

    private $content;

    /**
     * {@inheritdoc}
     */
    public static function getMode()
    {
        return self::STRING_MODE;
    }

    /**
     * {@inheritdoc}
     */
    public static function getResponseType()
    {
        return 'json';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse()
    {
        $response = $this->getResponse();
        $this->arraySortByColumn($response, 'price');
        return $response;
    }

    /**
     * Simulate get response method
     *
     * @return string
     */
    protected function getResponse()
    {
        return json_decode($this->content, true);
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

}
