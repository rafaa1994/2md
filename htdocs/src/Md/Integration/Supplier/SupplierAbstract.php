<?php

namespace Md\Integration\Supplier;

/**
 * Class SupplierAbstract
 * @package Md\Integration\Supplier
 */
abstract class SupplierAbstract implements SupplierInterface
{

    /**
     * Parse response
     *
     * @return array
     */
    abstract protected function parseResponse();

    /**
     * {@inheritdoc}
     */
    abstract public function setContent($content);

    /**
     * {@inheritdoc}
     */
    public function getProducts()
    {
        return $this->parseResponse();
    }

    /**
     * Sort array
     * @param $array
     * @param $column
     * @param int $sort
     */
    protected function arraySortByColumn(&$array, $column, $sort = SORT_ASC) {
        $sortableCol = [];
        foreach ($array as $key=> $row) {
            $sortableCol[$key] = $row[$column];
        }
        array_multisort($sortableCol, $sort, $array);
    }
}
