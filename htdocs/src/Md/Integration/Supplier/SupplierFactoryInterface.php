<?php

namespace Md\Integration\Supplier;

/**
 * Interface SupplierFactoryInterface
 * @package Md\Integration\Supplier
 */
interface SupplierFactoryInterface
{

    /**
     * Get supplier
     *
     * @param string $supplierName
     *
     * @throws \InvalidArgumentException
     * @return SupplierInterface
     */
    public function getSupplier($supplierName);
}
