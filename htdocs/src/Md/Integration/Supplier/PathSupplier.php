<?php

namespace Md\Integration\Supplier;

/**
 * Class PathSupplier
 * @package Md\Integration\Supplier
 */
class PathSupplier extends SupplierAbstract
{

    const PATH_MODE = 2;

    public $content;

    /**
     * {@inheritdoc}
     */
    public static function getMode()
    {
        return self::PATH_MODE;
    }

    /**
     * {@inheritdoc}
     */
    public static function getResponseType()
    {
        return 'json';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse()
    {
        $response = $this->getResponse();
        $this->arraySortByColumn($response, 'price');
        return $response;
    }

    /**
     * Simulate get response method
     * @return string
     * @throws \Exception
     */
    protected function getResponse()
    {
        $fileContent = file_get_contents($this->content);
        if ($fileContent) {
            return json_decode($fileContent, true);
        }
        throw new \Exception('File not found.');
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
