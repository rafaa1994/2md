<?php

namespace Md\Integration\Helper;

/**
 * Class Comparer
 * @package Md\Integration\Helper
 */
class Comparer
{

    /**
     * Check if contain number of params are the same in haystack
     * @param string $haystack
     * @param array $params
     * @return bool
     */
    public static function equalSubstringNumber($haystack, $params)
    {
        $perform = [];
        foreach ($params as $param) {
            $perform[$param] = substr_count($haystack, $param);
        }
        return count(array_unique($perform)) === 1;
    }
}