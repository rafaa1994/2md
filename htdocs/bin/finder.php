<?php

require __DIR__.'/../vendor/autoload.php';

use Md\Integration\Command\SortingJSON;
use Md\Integration\Command\StringComparer;
use Md\Integration\Supplier\SupplierFactory;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new StringComparer());
$application->add(new SortingJSON(null, new SupplierFactory()));
$application->run();