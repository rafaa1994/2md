# RECRUITMENT TASK

### Using Symfony framework and Console component please create two CLI commands: 

# HOW TO RUN

Application is realized task prepared on the description on the bottom.

## Requirments
- PHP 7.0 or higher
- composer 1.4.0 or higher
- Apache2 (optional)

**If you are linux user you can use bootstrap.sh to install require tools.**


To run this by console copy project on your driver. Run `composer install` in **htdocs** directory and then you need to open console in project directory **bin**
and run commands:

1. 

```
md:comparer <haystack> [<needles>]...

Arguments:
  haystack              String for check
  needles               Optional parameters. Default is set "Mary" and "John"

example $ md:comparer MaryMaryJohnMarcel Mary John Marcel

result should be : __Number of times is the same : 0__
```

2. 

```
Usage:
  md:sorting <mode> <supplier>

Arguments:
  mode                  1 --String in JSON param	2 --Path to JSON file
  supplier              Data param

```

There are provided 2 mods. First you can add data JSON as string parameter

```

php finder.php md:sorting 1 '[{"title":"H&M T-Shirt White","price":10.99,"inventory":10},{"title":"Magento Enterprise License","price":1999.99,"inventory":9999},{"title":"iPad 4 Mini","price":500.01,"inventory":2},{"title":"iPad Pro","price":990.2,"inventory":2},{"title":"Garmin Fenix 5","price":789.67,"inventory":34},{"title":"Garmin Fenix 3 HR Sapphire Performer Bundle","price":789.67,"inventory":12}]'

or

php finder.php md:sorting 2 "../web/supplier.json"


```

Both the result should be :


```

+---------------------------------------------+---------+-----------+
| title                                       | price   | inventory |
+---------------------------------------------+---------+-----------+
| H&M T-Shirt White                           | 10.99   | 10        |
| iPad 4 Mini                                 | 500.01  | 2         |
| Garmin Fenix 3 HR Sapphire Performer Bundle | 789.67  | 12        |
| Garmin Fenix 5                              | 789.67  | 34        |
| iPad Pro                                    | 990.2   | 2         |
| Magento Enterprise License                  | 1999.99 | 9999      |
+---------------------------------------------+---------+-----------+

JSON format
[{"title":"H&M T-Shirt White","price":10.99,"inventory":10},{"title":"iPad 4 Mini","price":500.01,"inventory":2},{"title":"Garmin Fenix 3 HR Sapphire Performer Bundle","price":789.67,"inventory":12},{"title":"Garmin Fenix 5","price":789.67,"inventory":34},{"title":"iPad Pro","price":990.2,"inventory":2},{"title":"Magento Enterprise License","price":1999.99,"inventory":9999}]


```



# TASK WHICH IS REALIZED BY APP

1. Command must take a string parameter containing text and will check if “John” and “Mary” names are found the same number of times inside the provided text. This test should be case insensitive. If the number of times is the same it should return 1, if not it should return 0. Please add unit tests if you can.


2. Command must take a string parameter containing array of products in JSON. It should return array of JSON string with products sorted by price ascending, and if price is the same sorted alphabetically ascending.
Sample JSON parameter: 
```
[	
	{
		"title": "H&M T-Shirt White",
		"price": 10.99,
		"inventory": 10
	},
	{
		"title": "Magento Enterprise License",
		"price": 1999.99,
		"inventory": 9999
	},
	{
		"title": "iPad 4 Mini",
		"price": 500.01,
		"inventory": 2
	},
	{
		"title": "iPad Pro",
		"price": 990.20,
		"inventory": 2
	},
	{
		"title": "Garmin Fenix 5",
		"price": 789.67,
		"inventory": 34
	},
	{
		"title": "Garmin Fenix 3 HR Sapphire Performer Bundle",
		"price": 789.67,
		"inventory": 12
	}
]
```

### Acceptance criteria

1. Code delivered in form on git repository (bitbucket, github)
2. Provide basic documentation (English) in markdown format how to run / test the code

