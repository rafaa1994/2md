#!/usr/bin/env bash

sudo apt-get -y install php7.0
sudo cp /vagrant/000-default.conf /etc/apache2/sites-enabled
sudo service apache2 restart
